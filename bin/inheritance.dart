
mixin Layup {
  void layup() => print('Can Layup'); 
}

mixin Threepoint{
  void threepoint() => print('can shoot threepoint high value');
}

mixin Midrange{
  void midrange() => print('can shoot midrange high value');
}

mixin Dunk{
  void dunk() => print('can Dunk');
}

mixin FreeThrow{
  void freeThrow() => print('can shoot FreeThrow high value');
}

mixin Floater{
  void floater() => print('can shoot Floater high value');
}

mixin Pullup{
  void pullup() => print('can shoot Pull-up high value');
}

mixin Hookshot{
  void hookshot() => print('can shoot Hookshot high value');
}

mixin Run{
  void run() => print('can Run');
}

mixin Jump{
  void jump() => print('can Jump');
}

mixin Rebound {
  void rebound() => print('Can Jump to get Ball on Air'); 
}

mixin Steal{
  void steal() => print('can steal Ball from other team');
}

mixin Block{
  void block() => print('can Eject Ball When ohter team Make Score');
}

abstract class Player with Run, Jump {
  void player(){
    run();
    jump();
  }
}

abstract class DEFENSIVE  extends Player with Rebound, Steal, Block{
  @override
  void LockdownDefender () {
    print("Good Defend the best offensive player on the other team");
  }

  @override
  void RimProtector () {
    print("Good Defend the Rim");
  }

  @override
  void ThreAndDefend () {
    print("Good Defend Perimeter and Find Open Space Catch&Shoot");
  }
}

abstract class OFFENSIVE extends Player with Layup, Dunk, Threepoint, Midrange, FreeThrow, Floater, Hookshot, Pullup{
  @override
  void Slasher() {
    print("Drive to score at Paint");
  }

  @override
  void Playmaker () {
    print("Good Passing");
  }

  @override
  void SpotUpShooter () {
    print("Good Find Open Space Catch&Shoot");
  }

  @override
  void Isolation () {
    print("Good One on One Play");
  }

  @override
  void PostMaster () {
    print("Good Post Pass or Socre Play");
  }

  @override
  void Cut () {
    print("Good Find Open Space to Score at Paint");
  }

  @override
  void Screen () {
    print("Help Team to Create Space to Make Play");
  }
}

class Foul {
  void foul() {
    print("Get Foul");
  }
}

class FragrantFoulOne implements Foul {
  @override
  void foul() {
    print("Get FragrantFoul 1");
  }
}

class FragrantFoulTwo implements Foul {
  @override
  void foul() {
    print("Get FragrantFoul 2");
  }
}

class John extends OFFENSIVE {
  void offen(){
    var p = John();
    p.player();
    p.layup();
    p.dunk();
    p.Slasher();
    p.Playmaker();
  }
}

class John1 extends DEFENSIVE {
  void defen(){
    var p = John1();
    p.rebound();
    p.RimProtector();
    // FragrantFoulOne f1 = FragrantFoulOne();
    // f1.foul();
    // Foul f3 = Foul();
    // f3.foul();
  }
}

class John2 extends Foul {
  void foul(){
    FragrantFoulOne f1 = FragrantFoulOne();
    f1.foul();
    Foul f2 = Foul();
    f2.foul();
  }
}

class Pete extends OFFENSIVE {
  void offen(){
    var p = Pete();
    p.player();
    p.layup();
    p.dunk();
    p.Slasher();
    p.Isolation();
  }
}

class Jalen extends OFFENSIVE{
  void offen(){
    var p = Jalen();
    p.player();
    p.layup();
    p.dunk();
    p.SpotUpShooter();
  }
}

class Allen extends OFFENSIVE{
  void offen(){
    var p = Allen();
    p.player();
    p.layup();
    p.dunk();
    p.PostMaster();
  }
}

void main(){
  var a = John();
  var a1 = John1();
  var a2 = John2();
  print('-------John-------');
  a.offen();
  a1.defen();
  a2.foul();

  var b = Pete();
  print('-------Pete-------');
  b.offen();

  var c = Jalen();
  print('-------Jalen-------');
  c.offen();

  var d = Allen();
  print('-------Allen-------');
  d.offen();
}