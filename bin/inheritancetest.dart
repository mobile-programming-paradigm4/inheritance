// mixin Layup {
//   void layup() => print('Can Layup'); 
// }

// mixin Threepoint{
//   void threepoint() => print('can shoot threepoint high value');
// }

// mixin Midrange{
//   void midrange() => print('can shoot midrange high value');
// }

// mixin Dunk{
//   void dunk() => print('can Dunk');
// }

// mixin FreeThrow{
//   void freeThrow() => print('can shoot FreeThrow high value');
// }

// mixin Floater{
//   void floater() => print('can shoot Floater high value');
// }

// mixin Pullup{
//   void pullup() => print('can shoot Pull-up high value');
// }

// mixin Hookshot{
//   void hookshot() => print('can shoot Hookshot high value');
// }

// mixin Run{
//   void run() => print('can Run');
// }

// mixin Jump{
//   void jump() => print('can Jump');
// }

// mixin Rebound {
//   void layup() => print('Can Jump to get Ball on Air'); 
// }

// mixin Steal{
//   void threepoint() => print('can steal Ball from other team');
// }

// mixin Block{
//   void dunk() => print('can Eject Ball When ohter team Make Score');
// }

// abstract class Player with Run, Jump {
//   void player(){
//     run();
//     jump();
//   }
// }

// abstract class DEFENSIVE  extends Player with Rebound, Steal, Block{
//   @override
//   void LockdownDefender () {
//     print("Good Defend the best offensive player on the other team");
//   }

//   @override
//   void RimProtector () {
//     print("Good Defend the Rim");
//   }

//   @override
//   void ThreAndDefend () {
//     print("Good Defend Perimeter and Find Open Space Catch&Shoot");
//   }
// }

// abstract class OFFENSIVE extends Player with Layup, Dunk, Threepoint, Midrange, FreeThrow, Floater, Hookshot, Pullup{
//   @override
//   void Slasher() {
//     print("Drive to score at Paint");
//   }

//   @override
//   void Playmaker () {
//     print("Good Passing");
//   }

//   @override
//   void SpotUpShooter () {
//     print("Good Find Open Space Catch&Shoot");
//   }

//   @override
//   void Isolation () {
//     print("Good One on One Play");
//   }

//   @override
//   void PostMaster () {
//     print("Good Post Pass or Socre Play");
//   }

//   @override
//   void Cut () {
//     print("Good Find Open Space to Score at Paint");
//   }

//   @override
//   void Screen () {
//     print("Help Team to Create Space to Make Play");
//   }
// }

// class John extends OFFENSIVE {
//   void offen(){
//     var p = John();
//     p.player();
//     p.layup();
//     p.dunk();
//     p.Slasher();
//     p.Playmaker();
//   }
// }

// class John1 extends DEFENSIVE {
//   void defen(){
//     var p = John1();
//     p.RimProtector();
//   }
// }

// class Pete extends OFFENSIVE {
//   void offen(){
//     var p = Pete();
//     p.player();
//     p.layup();
//     p.dunk();
//     p.Slasher();
//     p.Isolation();
//   }
// }

// class Jalen extends OFFENSIVE{
//   void offen(){
//     var p = Jalen();
//     p.player();
//     p.layup();
//     p.dunk();
//     p.SpotUpShooter();
//   }
// }

// class Allen extends OFFENSIVE{
//   void offen(){
//     var p = Allen();
//     p.player();
//     p.layup();
//     p.dunk();
//     p.PostMaster();
//   }
// }

// void main(){
//   var a = John();
//   var a1 = John1();
//   print('-------John-------');
//   a.offen();
//   a1.defen();

//   var b = Pete();
//   print('-------Pete-------');
//   b.offen();

//   var c = Jalen();
//   print('-------Jalen-------');
//   c.offen();

//   var d = Allen();
//   print('-------Allen-------');
//   d.offen();
// }


class Interface {
  void display() {
    print(" This is the function of the interface class. ");
  }
}

class Subclass implements Interface {
  @override
  void display() {
    print(" This is the function of the subclass. ");
  }
}

// abstract class Teacher {
//   void say();
//   void write();
// }

// class Student extends Teacher {
//   @override
//   void say() {
//     print(
//         " Overriding the abstract method say( ) of the base class in the derived class ! ");
//   }

//   @override
//   void write() {
//     print(
//         " Overriding the abstract method write( ) of the base class in the derived class ");
//   }
// }

// mixin Swim {
//   void swim() => print('Swimming'); 
// }

// mixin Bite{
//   void bite() => print('Chomp');
// }

// mixin Crawl{
//   void crawl() => print('Crawling');
// }

// abstract class Reptite with Swim, Bite, Crawl{
//   void hunt() {
//     //print('Alligator -------');
//     swim();
//     crawl();
//     bite();
//     print('Eat Fish');
//   }
// }

// class Alligator extends Reptite{}

// class Crocodile extends Reptite {}

// class Fish with Swim, Bite {}


void main(List<String> arguments) {
  Subclass s1 = Subclass();
  s1.display();
  Interface s2 = Interface();
  s2.display();
}

// // int main() {
// //   Student s1 = Student();
// //   s1.say();
// //   s1.write();

// //   return 0;
// // }

// // void main(){
// //   print('Alligator -------');
// //   var all1 = Alligator();
// //   all1.hunt();
// //   print('Crocodile -------');
// //   var cro1 = Crocodile();
// //   cro1.hunt();
// //   print('Fish -------');
// //   var fis1 = Fish();
// //   fis1.swim();
// //   fis1.bite();
// // }